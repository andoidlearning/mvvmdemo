package com.example.mvvmdemoapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider

class MainActivity : AppCompatActivity() {
    private lateinit var booksViewModel: BooksViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val tvBookNames: TextView = findViewById(R.id.bookDetailTextView)

        booksViewModel = ViewModelProvider(this).get(BooksViewModel::class.java)

        booksViewModel.booksLiveData.observe(this){ books ->

            Log.i("AllBooks",books.toString())

            // Concatenate the book names into a single string
            val bookNames = books.joinToString(separator = "\n") {
                "${it.bookName} by ${it.bookAuthor} - Price:- ${it.bookPrice}"
            }

            // Update the TextView with the book names
            tvBookNames.text = bookNames
        }
    }
}