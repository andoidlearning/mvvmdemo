package com.example.mvvmdemoapp

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class BooksViewModel : ViewModel() {
    val booksLiveData = MutableLiveData<List<Book>>()

    init {
        getAllBooks()
    }

    fun getAllBooks(){
        val allBooks = Repo.getAllBooks()
        booksLiveData.value = allBooks
    }
}