package com.example.mvvmdemoapp

object Repo {

    fun getAllBooks() = listOf<Book>(

        Book(
            bookName = "Alchemist",
            bookPrice = "300",
            bookGenre = "Motivational",
            bookAuthor = "Paulo Coelho"
        ),

        Book(
            bookName = "Panipath",
            bookPrice = "450",
            bookGenre = "Histrolical",
            bookAuthor = "Vishvas Patil"
        ),

        Book(
            bookName = "Chawa",
            bookPrice = "800",
            bookGenre = "Histrolical",
            bookAuthor = "Shivaji Sawant"
        )
    )

}