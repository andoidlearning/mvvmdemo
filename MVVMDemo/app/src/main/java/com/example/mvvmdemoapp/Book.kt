package com.example.mvvmdemoapp

data class Book(
    val bookName : String,
    val bookPrice : String,
    val bookAuthor : String,
    val bookGenre : String
)